# vfs tools

A crate with a few tools to make the use of [vfs](https://docs.rs/vfs/0.9.0/vfs/)
a bit more convenient.

## Macros

[setup_files] is a macro for preparing a MemoryFS with contents.
This macro is intended for testing.

```rust
use vfs_tools::setup_files;
let root_path = setup_files! {
  some/path/"file1": b"contents of file1"
  some/path/"file2": b"contents of file2"
  some/other/path/"file3": b"contents of file3"
}?;
let mut result = String::new();
root_path.join("some/path/file2")?.open_file()?.read_to_string(&mut result)?;
assert_eq!("contents of file2", result);
# Ok::<(),::vfs::error::VfsError>(())
```
## extensions

[VfsPathExt] contains extensions to [vfs::VfsPath]

