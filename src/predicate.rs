use vfs::{VfsPath, VfsResult};

pub(crate) fn apply_predicate<P>(path:VfsPath, predicate:P) -> VfsResult<Option<VfsPath>>
    where P:Fn(&VfsPath) -> VfsResult<bool>,
{
    match predicate(&path)? {
        true => Ok(Some(path)),
        false => Ok(None),
    }
}

pub(crate) fn file_only(path: VfsPath) -> VfsResult<Option<VfsPath>> {
    apply_predicate(path, VfsPath::is_file)
}

pub(crate) fn dir_only(path: VfsPath) -> VfsResult<Option<VfsPath>> {
    apply_predicate(path, VfsPath::is_dir)
}

#[cfg(test)]
fn setup_files() -> VfsResult<VfsPath> {
    crate::setup_files! {
        dir/subdir/"file.txt" : b"change files"
    }
}

#[test]
fn file_only_returns_none_for_dir() -> VfsResult<()> {
    let root = setup_files()?;
    assert!(file_only(root.join("dir")?)?.is_none());
    Ok(())
}

#[test]
fn file_only_returns_some_for_file() -> VfsResult<()> {
    let root = setup_files()?;
    assert!(file_only(root.join("dir")?.join("subdir")?.join("file.txt")?)?.is_some());
    Ok(())
}
#[test]
fn dir_only_returns_some_for_dir() -> VfsResult<()> {
    let root = setup_files()?;
    assert!(dir_only(root.join("dir")?)?.is_some());
    Ok(())
}

#[test]
fn dir_only_returns_none_for_file() -> VfsResult<()> {
    let root = setup_files()?;
    assert!(dir_only(root.join("dir")?.join("subdir")?.join("file.txt")?)?.is_none());
    Ok(())
}
